﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Actors;

namespace SampleService2.Interfaces
{
    public interface ISampleService2 : IActor
    {
        Task<string> DoWorkAsync();
    }
}
