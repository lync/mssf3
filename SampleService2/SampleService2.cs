﻿using SampleService2.Interfaces;
using Microsoft.ServiceFabric.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleService2
{
    /// <remarks>
    /// Each ActorID maps to an instance of this class.
    /// The ISampleService2 interface (in a separate DLL that client code can
    /// reference) defines the operations exposed by SampleService2 objects.
    /// </remarks>
    internal class SampleService2 : StatelessActor, ISampleService2
    {
        Task<string> ISampleService2.DoWorkAsync()
        {
            // TODO: Replace the following with your own logic.
            ActorEventSource.Current.ActorMessage(this, "Doing Work");

            return Task.FromResult("Work result");
        }
    }
}
