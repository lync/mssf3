﻿using SampleService1.Interfaces;
using Microsoft.ServiceFabric.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleService1
{
    /// <remarks>
    /// Each ActorID maps to an instance of this class.
    /// The ISampleService1 interface (in a separate DLL that client code can
    /// reference) defines the operations exposed by SampleService1 objects.
    /// </remarks>
    internal class SampleService1 : StatelessActor, ISampleService1
    {
        Task<string> ISampleService1.DoWorkAsync()
        {
            // TODO: Replace the following with your own logic.
            ActorEventSource.Current.ActorMessage(this, "Doing Work");

            return Task.FromResult("Work result");
        }
    }
}
